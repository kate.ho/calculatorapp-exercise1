package com.example.calculatorapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.calculatorapp.util.util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.tv_screen)
    TextView tvScreen;
    @BindView(R.id.btn_clear)
    Button btnClear;
    @BindView(R.id.btn_equal)
    Button btnEqual;

    private String mNumber;
    private int mSign;
    private int mFirstNumber;
    private int mLastNumber;
    private int mResult;
    private SharedPreferences PREFSaveResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        PREFSaveResult = getSharedPreferences("myprefer",MODE_PRIVATE);
        btnEqual.setBackgroundColor(Color.RED);

        if(PREFSaveResult.getInt("result",0) != 0) {
            tvScreen.setText(String.valueOf(PREFSaveResult.getInt("result", 0)));
        }else {
            tvScreen.setText("0");
        }
    }

    @OnClick(R.id.btn_number_0)
    public void onNumber0Click(View view) {
        mNumber = ((mNumber != null) ? mNumber + "0" : "0");
        tvScreen.setText(mNumber);
    }

    @OnClick(R.id.btn_number_1)
    public void onNumber1Click(View view) {
        mNumber = ((mNumber != null) ? mNumber + "1" : "1");
        tvScreen.setText(mNumber);
    }

    @OnClick(R.id.btn_number_2)
    public void onNumber2Click(View view) {
        mNumber = ((mNumber != null) ? mNumber + "2" : "2");
        tvScreen.setText(mNumber);
    }

    @OnClick(R.id.btn_number_3)
    public void onNumber3Click(View view) {
        mNumber = ((mNumber != null) ? mNumber + "3" : "3");
        tvScreen.setText(mNumber);
    }

    @OnClick(R.id.btn_number_4)
    public void onNumber4Click(View view) {
        mNumber = ((mNumber != null) ? mNumber + "4" : "4");
        tvScreen.setText(mNumber);
    }

    @OnClick(R.id.btn_number_5)
    public void onNumber5Click(View view) {
        mNumber = ((mNumber != null) ? mNumber + "5" : "5");
        tvScreen.setText(mNumber);
    }
    @OnClick(R.id.btn_number_6)
    public void onNumber6Click(View view) {
        mNumber = ((mNumber != null) ? mNumber + "6" : "6");
        tvScreen.setText(mNumber);
    }

    @OnClick(R.id.btn_number_7)
    public void onNumber7Click(View view) {
        mNumber = ((mNumber != null) ? mNumber + "7" : "7");
        tvScreen.setText(mNumber);
    }

    @OnClick(R.id.btn_number_8)
    public void onNumber8Click(View view) {
        mNumber = ((mNumber != null) ? mNumber + "8" : "8");
        tvScreen.setText(mNumber);
    }

    @OnClick(R.id.btn_number_9)
    public void onNumber9Click(View view) {
        mNumber = ((mNumber != null) ? mNumber + "9" : "9");
        tvScreen.setText(mNumber);
    }

    @OnClick(R.id.btn_division)
    public void onDivisionClick(View view) {
        if (mNumber != null){
            mFirstNumber = Integer.parseInt(mNumber);
            mSign = util.DISITION;
            mNumber = null;
        }else if (mSign != 0){
            mSign = util.DISITION;
        }
    }

    @OnClick(R.id.btn_multiplication)
    public void onMultiplicationClick(View view) {
        if (mNumber != null){
            mFirstNumber = Integer.parseInt(mNumber);
            mSign = util.MULTIPLICATION;
            mNumber = null;
        }else if(mSign != 0){
            mSign = util.MULTIPLICATION;
        }
    }

    @OnClick(R.id.btn_subtraction)
    public void onSubtractionClick(View view) {
        if (mNumber != null){
            mFirstNumber = Integer.parseInt(mNumber);
            mSign = util.SUBTRACTION;
            mNumber = null;
        }else if(mSign != 0){
            mSign = util.SUBTRACTION;
        }
    }

    @OnClick(R.id.btn_plus)
    public void onPlusClick(View view) {
        if (mNumber != null){
            mFirstNumber = Integer.parseInt(mNumber);
            mSign = util.PLUS;
            mNumber = null;
        }else if(mSign != 0){
            mSign = util.PLUS;
        }
    }

    @OnClick(R.id.btn_clear)
    public void onClearClick(View view) {
       mNumber = null;
       tvScreen.setText("0");
        btnEqual.setBackgroundColor(Color.RED);
    }

    @OnClick(R.id.btn_equal)
    public void onEqualClick(View view) {
        if (mFirstNumber != 0 && mNumber != null){
            mLastNumber = Integer.parseInt(mNumber);
            switch (mSign){
                case util.DISITION: mResult = mFirstNumber / mLastNumber;
                    break;
                case util.MULTIPLICATION: mResult = mFirstNumber * mLastNumber;
                    break;
                case util.SUBTRACTION: mResult = mFirstNumber - mLastNumber;
                    break;
                case util.PLUS: mResult = mFirstNumber + mLastNumber;
                    break;
            }

            tvScreen.setText(String.valueOf(mResult));
            mNumber = String.valueOf(mResult);
            mFirstNumber = 0;
            mLastNumber = 0;
            mNumber = null;
            btnEqual.setBackgroundColor(Color.GREEN);
            SharedPreferences.Editor editor = PREFSaveResult.edit();
            editor.putInt("result", mResult);
            editor.commit();
        }
    }
}
